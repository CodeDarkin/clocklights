# Scrolling Clock Lights Triggered by Motion Detection

## Clock lights using Neopixel lights, PIR Motion Sensor, and Python script

Create this DIY scrolling clock with your LED lights!  This script currently has the clock turn on when motion is detected for approximately two scrolls
before shutting off.

The neopixel light strips were bent to make 8 rows, 30 pixels per row.

[Hardware Schematics for LED strip](https://www.thegeekpub.com/15990/wiring-ws2812b-addressable-leds-to-the-raspbery-pi/)

[Wiring for Motion Sensor](https://projects.raspberrypi.org/en/projects/physical-computing/11)

## Software Requirements
`sudo pip3 install rpi_ws281x adafruit-circuitpython-neopixel`

### If you would like the program to be on whenever the Raspberry Pi is powered on
`sudo nano /etc/profile`

Then add the command to run the Python script at the bottom of the file. Be sure to change the path to your script if needed

`sudo python3 ~/clocklights.py`
