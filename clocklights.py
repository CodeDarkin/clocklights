import board
import neopixel
import datetime, pytz, time
from gpiozero import MotionSensor

#script to display and scroll current time on neopixel lights

pixels = neopixel.NeoPixel(board.D18, 240, auto_write=False)
#turn all lights one color
def one_color(tup):
  for i in range(0, 240):
    pixels[i] = tup

#turn off all lights
def off():
  one_color((0,0,0))

#functions to represent clock characters
def space():
  return[" ",
         " ",
         " ",
         " ",
         " "]
def nine():
  return ["xxx",
          "x x",
          "xxx",
          "  x",
          "  x",]

def eight():
  return ["xxx",
	  "x x",
	  "xxx",
          "x x",
          "xxx"]

def seven():
  return ["xxx",
          "  x",
          "  x",
          "  x",
          "  x",]

def six():
  return ["xxx",
          "x  ",
          "xxx",
          "x x",
          "xxx"]

def five():
  return ["xxx",
          "x  ",
          "xxx",
          "  x",
          "xxx"]

def four():
  return ["x x",
          "x x",
          "xxx",
          "  x",
          "  x"]

def three():
  return ["xxx",
          "  x",
          "xxx",
          "  x",
          "xxx"]

def two():
  return ["xxx",
          "  x",
          "xxx",
          "x  ",
          "xxx"]

def one():
  return ["x",
          "x",
          "x",
          "x",
          "x"]

def zero():
  return ["xxx",
          "x x",
          "x x",
          "x x",
          "xxx"]

def colon():
  return [" ",
          "x",
          " ",
          "x",
          " "]

#creates frame above and below the time display
def renderbox(pixels_per_row):
  total_rows=8
  tup=(46,91,91)
  for pixel in range(0, pixels_per_row):
    pixels[pixel]=tup
    pixels[(total_rows-1)*pixels_per_row+pixel]=tup

#joins together clock characters
def concatenate(matrices):
  output=['','','','','']
  for row in range(0,5):
    for matrix in matrices:
      output[row]=output[row] + ' ' + matrix[row]
  return output

#renders clock characters to neopixel lights
def render(matrices):
  pixelsperx = 1
  renderblock=concatenate(matrices)
  pixels_per_row = 30
  renderbox(pixels_per_row)
  for row in range(4,-1,-1):
    for column in range(0,len(renderblock[row])):
#whenever you see pixels, refer to physical address
     if row % 2 == 1:
       phys_col=column*pixelsperx
       invert_phys=1
#odd row
     else:
       phys_col=pixels_per_row-column*pixelsperx-3
       invert_phys=-1
     physicaladdr= pixels_per_row*(5-row)+ (phys_col)
     if 'x' == renderblock[row][column]:
       color = (30,30,30)
     else:
       color = (0,0,0)
     pixels[physicaladdr] = color
  pixels.show()

#gets current time in NY and returns string hours and minutes
def gettime():
    NYtime=pytz.timezone("America/New_York")
    currenttime = datetime.datetime.now(tz=NYtime)
    hours = str(currenttime.hour)
    minutes = currenttime.minute
    minutes = f'{minutes:02}'
    return (hours, minutes)

#collects clock characters given the current time, calls render function, and scrolls
def clockloop(loops):
 scroll_position=0
 for x in range(0,loops):
  try:
    scroll_position+=1
    if scroll_position > 6:
      scroll_position = 0
    off()
    (hours, minutes)=gettime()
    num_match= {0:zero(), 1:one(), 2:two(), 3:three(), 4:four(), 5:five(), 6:six(), 7:seven(), 8:eight(), 9:nine(), ":":colon()}
    render_list=[]
    for sp in range(0,scroll_position):
        render_list.append(space())
    for digit in hours:
        render_list.append(num_match[int(digit)])
    render_list.append(colon())
    for digit in minutes:
        render_list.append(num_match[int(digit)])
    render(render_list)

    pixels.show()
    time.sleep(1)

  except KeyboardInterrupt:
    off()
    pixels.show()
    raise
 off()
 pixels.show()

#motion sensor detects movement and activates clock for a specified time period
if __name__ == "__main__":
  pir = MotionSensor(4)
  while True:
    if pir.motion_detected:
        clockloop(16)



